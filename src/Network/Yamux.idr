module Network.Yamux

import Network.Socket
import public Network.Yamux.Data
import public Network.Yamux.Impl

%include C "yamux.h"
%include C "idr_yamux.h"

%link C "frame.o"
%link C "session.o"
%link C "stream.o"
%link C "idr_yamux.o"

%access export

private
mkErr : Int -> IO (Maybe ErrVal)
mkErr 0 = pure Nothing
mkErr n = do
  e <- getErrno
  pure $ Just $ MkErrVal n e
private
errIfN : Int -> IO (Maybe ErrVal)
errIfN n =
  if n < 0 then do
    e <- getErrno
    pure $ Just $ MkErrVal n e
  else pure Nothing

config : Session -> Config
config (MkSession p) =
  unsafePerformIO $ do
    pCfg <- foreign FFI_C "idr_yamux_session_config"        (Ptr -> IO Ptr) p
    accb <- foreign FFI_C "idr_yamux_config_accept_backlog" (Ptr -> IO Int) pCfg
    msws <- foreign FFI_C "idr_yamux_config_msws"           (Ptr -> IO Int) pCfg
    pure $ MkConfig accb msws

numStreams : Session -> Int
numStreams (MkSession p) = unsafePerformIO $ foreign FFI_C "idr_yamux_session_num_streams" (Ptr -> IO Int) p
capStreams : Session -> Int
capStreams (MkSession p) = unsafePerformIO $ foreign FFI_C "idr_yamux_session_cap_streams" (Ptr -> IO Int) p
streams : Session -> List Stream
streams (MkSession p) =
  unsafePerformIO $ do
    arr <- foreign FFI_C "idr_yamux_session_get_stream_arr"  (Ptr -> IO Ptr) p
    del <- foreign FFI_C "idr_yamux_session_get_stream_size" (()  -> IO Int) ()
    lst <- mkStreamList [] (capStreams $ MkSession p) arr del
    pure $ reverse lst
  where
    getList : List Stream -> Int -> Ptr -> List Stream
    getList l 0 _ = l
    getList l _ s = (MkStream s) :: l
    mkStreamList : List Stream -> Int -> Ptr -> Int -> IO (List Stream)
    mkStreamList l 0 _ _ = pure l
    mkStreamList l n p d = do
      active <- foreign FFI_C "idr_yamux_session_stream_get_active" (Ptr -> IO Int) p
      stream <- foreign FFI_C "idr_yamux_session_stream_get_stream" (Ptr -> IO Ptr) p
      assert_total $ mkStreamList (getList l active stream) (n - 1) (p `prim__ptrOffset` d) d

type : Session -> SessionType
type (MkSession p) = cast $ unsafePerformIO $ foreign FFI_C "idr_yamux_session_type" (Ptr -> IO Int) p
isClosed : Session -> Bool
isClosed (MkSession p) = (unsafePerformIO $ foreign FFI_C "idr_yamux_session_closed" (Ptr -> IO Int) p) /= 0

mkSession : Config -> Socket -> SessionType -> IO Session
mkSession cfg sock typ = do
  foreign FFI_C "idr_yamux_session_new" (Int -> Int -> Int -> Int -> IO Ptr)
              (acceptBacklog cfg) (maxStreamWindowSize cfg) (descriptor sock) (cast typ)
    >>= pure . MkSession
freeSession : Session -> IO ()
freeSession (MkSession p) = foreign FFI_C "yamux_session_free" (Ptr -> IO ()) p

closeSession : Session -> ProtoError -> IO (Maybe ErrVal)
closeSession (MkSession p) pe =
  foreign FFI_C "yamux_session_close" (Ptr -> Int -> IO Int) p (cast pe) >>= errIfN
%inline
goAway : Session -> ProtoError -> IO (Maybe ErrVal)
goAway s e = closeSession s e

ping : Session -> Int -> IO (Maybe ErrVal)
ping (MkSession p) v = foreign FFI_C "idr_yamux_session_ping" (Ptr -> Int -> IO Int) p v >>= errIfN
pong : Session -> Int -> IO (Maybe ErrVal)
pong (MkSession p) v = foreign FFI_C "idr_yamux_session_pong" (Ptr -> Int -> IO Int) p v >>= errIfN

read : Session -> IO (Maybe ErrVal)
read (MkSession p) = foreign FFI_C "yamux_session_read" (Ptr -> IO Int) p >>= errIfN

-- TODO: callbacks


session : Stream -> Session
session (MkStream p) =
  MkSession $ unsafePerformIO $ foreign FFI_C "idr_yamux_stream_session" (Ptr -> IO Ptr) p
state : Stream -> StreamState
state (MkStream p) =
  cast $ unsafePerformIO $ foreign FFI_C "idr_yamux_stream_state" (Ptr -> IO Int) p
streamId : Stream -> Int
streamId (MkStream p) =
  unsafePerformIO $ foreign FFI_C "idr_yamux_stream_id" (Ptr -> IO Int) p
windowSize : Stream -> Int
windowSize (MkStream p) =
  unsafePerformIO $ foreign FFI_C "idr_yamux_stream_ws" (Ptr -> IO Int) p

mkStream : Session -> Maybe Int -> IO (Either ErrVal Stream)
mkStream (MkSession p) Nothing = assert_total $ mkStream (MkSession p) $ Just 0
mkStream (MkSession p) (Just id) = do
  pSt <- foreign FFI_C "idr_yamux_stream_new" (Ptr -> Int -> IO Ptr) p id
  eq <- foreign FFI_C "_idr_prim__ptr_is_null" (Ptr -> IO Int) pSt
  if eq /= 0
  then do
    e <- getErrno
    pure $ Left $ MkErrVal (-1) e
  else pure $ Right $ MkStream pSt

freeStream : Stream -> IO ()
freeStream (MkStream p) = foreign FFI_C "yamux_stream_free" (Ptr -> IO ()) p

initStream : Stream -> IO (Maybe ErrVal)
initStream  (MkStream p) = foreign FFI_C "yamux_stream_init"  (Ptr -> IO Int) p >>= errIfN
closeStream : Stream -> IO (Maybe ErrVal)
closeStream (MkStream p) = foreign FFI_C "yamux_stream_close" (Ptr -> IO Int) p >>= errIfN
resetStream : Stream -> IO (Maybe ErrVal)
resetStream (MkStream p) = foreign FFI_C "yamux_stream_reset" (Ptr -> IO Int) p >>= errIfN

windowUpdate : Stream -> Int -> IO (Maybe ErrVal)
windowUpdate (MkStream p) d = foreign FFI_C "idr_yamux_stream_wu" (Ptr -> Int -> IO Int) p d >>= mkErr

write : Stream -> String -> IO (Maybe ErrVal)
write (MkStream p) d =
  foreign FFI_C "idr_yamux_stream_write" (Ptr -> Int -> String -> IO Int)
    p (cast $ length d) d >>= errIfN

-- TODO: callbacks

