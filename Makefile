
PKG=yamux

default: build

check:
	idris --check $(PKG).ipkg

build:
	idris --build $(PKG).ipkg

install: #build
	idris --install $(PKG).ipkg

installdoc:
	idris --installdoc $(PKG).ipkg

clean:
	idris --clean $(PKG).ipkg

.PHONY: check build install clean
