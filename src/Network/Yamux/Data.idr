module Network.Yamux.Data

import Network.Socket

%access public export

record Config where
  constructor MkConfig

  acceptBacklog       : Int
  maxStreamWindowSize : Int

defaultConfig : Config
defaultConfig = MkConfig 0x100 $ 0x100 * 0x400

data FrameType  = Data | WindowUpdate | Ping | GoAway
data FrameFlags = Syn | Ack | Fin | Rst

record Frame where
  constructor MkFrame

  version  : Int
  type     : FrameType
  flags    : List FrameFlags
  streamId : Int
  length   : Int
  framData : String

data SessionType = Client | Server
data ProtoError  = Normal | Protocol | Internal
data StreamState = Initialised | SynSent | SynReceived | Established | Closing | Closed

record ErrVal where
  constructor MkErrVal

  returnVal : Int
  errnoVal  : Int

data Session = MkSession Ptr
data Stream  = MkStream  Ptr

PingCallback : Type
PingCallback = Session -> Int -> IO ()
PongCallback : Type
PongCallback = Session -> Int -> IO () -- TODO: how to timespec?
GoAwayCallback : Type
GoAwayCallback = Session -> ProtoError -> IO ()
NewStreamCallback : Type
NewStreamCallback = Session -> Stream -> IO ()

StreamReadCallback : Type
StreamReadCallback = Stream -> Int -> Ptr -> IO ()
StreamFinCallback : Type
StreamFinCallback = Stream -> IO ()
StreamRstCallback : Type
StreamRstCallback = StreamFinCallback

