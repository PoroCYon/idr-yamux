
#include "idr_yamux.h"

ssize_t _idr_prim__andInt_(ssize_t a, ssize_t b)
{
    return a & b;
}
ssize_t _idr_prim__orInt_(ssize_t a, ssize_t b)
{
    return a | b;
}
/*ssize_t _idr_prim__xorInt_(ssize_t a, ssize_t b)
{
    return a ^ b;
}*/
ssize_t _idr_prim__shlInt_(ssize_t a, ssize_t b)
{
    return a << b;
}
/*ssize_t _idr_prim__shrInt_(ssize_t a, ssize_t b)
{
    return a >> b;
}*/
ssize_t _idr_prim__ptr_is_null(void* foo)
{
    return foo == 0 ? 1 : 0;
}

struct yamux_config* idr_yamux_session_config(struct yamux_session* sess)
{
    return sess->config;
}

ssize_t idr_yamux_config_accept_backlog(struct yamux_config* cfg)
{
    return (ssize_t)cfg->accept_backlog;
}
ssize_t idr_yamux_config_msws(struct yamux_config* cfg)
{
    return (ssize_t)cfg->max_stream_window_size;
}

ssize_t idr_yamux_session_num_streams(struct yamux_session* sess)
{
    return (ssize_t)sess->num_streams;
}
ssize_t idr_yamux_session_cap_streams(struct yamux_session* sess)
{
    return (ssize_t)sess->cap_streams;
}

ssize_t idr_yamux_session_type(struct yamux_session* sess)
{
    return (ssize_t)sess->type;
}
ssize_t idr_yamux_session_closed(struct yamux_session* sess)
{
    return sess->closed ? 1 : 0;
}

ssize_t idr_yamux_stream_state(struct yamux_stream* str)
{
    return (ssize_t)str->state;
}
ssize_t idr_yamux_stream_id(struct yamux_stream* str)
{
    return (ssize_t)str->id;
}
ssize_t idr_yamux_stream_ws(struct yamux_stream* str)
{
    return (ssize_t)str->window_size;
}

static void on_session_free(struct yamux_session* sess)
{
    free(sess->config);
}
struct yamux_session* idr_yamux_session_new(ssize_t accB, ssize_t msws, ssize_t sock, ssize_t type)
{
    struct yamux_config* conf = (struct yamux_config*)malloc(sizeof(struct yamux_config));

    conf->accept_backlog = (size_t)accB;
    conf->max_stream_window_size = (uint32_t)msws;

    struct yamux_session* sess = yamux_session_new(conf, (int)sock, (enum yamux_session_type)type, NULL);

    sess->free_fn = on_session_free;

    return sess;
}

struct yamux_session_stream* idr_yamux_session_get_stream_arr(struct yamux_session* session)
{
    return session->streams;
}
ssize_t idr_yamux_get_session_stream_size()
{
    return (ssize_t)sizeof(struct yamux_session_stream);
}
ssize_t idr_yamux_session_stream_get_active(struct yamux_session_stream* yss)
{
    return yss->alive ? 1 : 0;
}
struct yamux_stream* yamux_session_stream_get_stream(struct yamux_session_stream* yss)
{
    return yss->stream;
}

// ---

struct yamux_session* idr_yamux_stream_session(struct yamux_stream* str)
{
    return str->session;
}

struct yamux_stream* idr_yamux_stream_new(struct yamux_session* session, ssize_t id)
{
    return yamux_stream_new(session, (yamux_streamid)id, NULL);
}
ssize_t idr_yamux_stream_wu(struct yamux_stream* stream, ssize_t dws)
{
    return yamux_stream_window_update(stream, (int32_t)dws);
}

ssize_t idr_yamux_stream_write(struct yamux_stream* stream, ssize_t size, char* data)
{
    return yamux_stream_write(stream, (uint32_t)size, data);
}

