
#ifndef IDR_YAMUX_H
#define IDR_YAMUX_H

#include <idris_rts.h>
#include <yamux.h>

ssize_t _idr_prim__andInt_(ssize_t a, ssize_t b);
ssize_t _idr_prim__orInt_ (ssize_t a, ssize_t b);
/*ssize_t _idr_prim__xorInt_(ssize_t a, ssize_t b);*/
ssize_t _idr_prim__shlInt_(ssize_t a, ssize_t b);
/*ssize_t _idr_prim__shrInt_(ssize_t a, ssize_t b);*/
ssize_t _idr_prim__ptr_is_null(void* foo);

struct yamux_config* idr_yamux_session_config(struct yamux_session* session);

ssize_t idr_yamux_config_accept_backlog(struct yamux_config* config);
ssize_t idr_yamux_config_msws          (struct yamux_config* config);

ssize_t idr_yamux_session_num_streams(struct yamux_session* session);
ssize_t idr_yamux_session_cap_streams(struct yamux_session* session);

ssize_t idr_yamux_session_type  (struct yamux_session* session);
ssize_t idr_yamux_session_closed(struct yamux_session* session);

ssize_t idr_yamux_stream_state(struct yamux_stream* stream);
ssize_t idr_yamux_stream_id   (struct yamux_stream* stream);
ssize_t idr_yamux_stream_ws   (struct yamux_stream* stream);

struct yamux_session* idr_yamux_session_new(ssize_t accB, ssize_t msws, ssize_t sock, ssize_t type);

ssize_t idr_yamux_session_ping(struct yamux_session* session, ssize_t val);
ssize_t idr_yamux_session_pong(struct yamux_session* session, ssize_t val);

struct yamux_session_stream* idr_yamux_session_get_stream_arr(struct yamux_session* session);
ssize_t idr_yamux_get_session_stream_size();
ssize_t idr_yamux_session_stream_get_active(struct yamux_session_stream* yss);
struct yamux_stream* yamux_session_stream_get_stream(struct yamux_session_stream* yss);

// ---

struct yamux_session* idr_yamux_stream_session(struct yamux_stream* stream);
ssize_t idr_yamux_stream_state(struct yamux_stream* stream);
ssize_t idr_yamux_stream_id(struct yamux_stream* stream);
ssize_t idr_yamux_stream_ws(struct yamux_stream* stream);

struct yamux_stream* idr_yamux_stream_new(struct yamux_session* session, ssize_t id);
ssize_t idr_yamux_stream_wu(struct yamux_stream* stream, ssize_t dws);

ssize_t idr_yamux_stream_write(struct yamux_stream* stream, ssize_t size, char* data);

#endif

