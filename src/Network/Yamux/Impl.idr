module Network.Yamux.Impl

import public Network.Yamux.Data

%access public export

Cast FrameType Int where
  cast Data         = 0x00
  cast WindowUpdate = 0x01
  cast Ping         = 0x02
  cast GoAway       = 0x03
Cast Int FrameType where
  cast 0x00 = Data
  cast 0x01 = WindowUpdate
  cast 0x02 = Ping
  cast 0x03 = GoAway
  cast _    = assert_unreachable

Cast FrameFlags Int where
  cast Syn = 0x0001
  cast Ack = 0x0002
  cast Fin = 0x0004
  cast Rst = 0x0008

Cast Int FrameFlags where
  cast 0x0000 = Syn
  cast 0x0001 = Ack
  cast 0x0002 = Fin
  cast 0x0003 = Rst
  cast _      = assert_unreachable

%inline
andInt : Int -> Int -> Int
andInt a b =
  unsafePerformIO $ foreign FFI_C "_idr_prim__andInt_" (Int -> Int -> IO Int) a b
%inline
orInt  : Int -> Int -> Int
orInt  a b =
  unsafePerformIO $ foreign FFI_C "_idr_prim__orInt_"  (Int -> Int -> IO Int) a b
%inline
shlInt : Int -> Int -> Int
shlInt a b =
  unsafePerformIO $ foreign FFI_C "_idr_prim__shlInt_" (Int -> Int -> IO Int) a b

Cast (List FrameFlags) Int where
  cast l = assert_total $ foldr orInt 0 $ map cast l

Cast Int (List FrameFlags) where
  cast 0 = []
  cast n =
    if (n `andInt` (n - 1)) == 0
    then [cast n]
    else map cast $ toPow2 n
    where
      tp2 : Int -> Int -> List Int -> List Int
      tp2 n i l =
        assert_total $
          let b  = n `andInt` (1 `shlInt` i) in
          let l' = if b == 0 then l else i :: l
          in tp2 n (i + 1) l'

      toPow2 : Int -> List Int
      toPow2 0 = []
      toPow2 n = tp2 n 0 []

Cast SessionType Int where
  cast Client = 0x00
  cast Server = 0x01
Cast Int SessionType where
  cast 0x00 = Client
  cast 0x01 = Server
  cast _    = assert_unreachable

Cast ProtoError Int where
  cast Normal   = 0x00
  cast Protocol = 0x01
  cast Internal = 0x02
Cast Int ProtoError where
  cast 0x00 = Normal
  cast 0x01 = Protocol
  cast 0x02 = Internal
  cast _    = assert_unreachable

Cast StreamState Int where
  cast Initialised = 0x00
  cast SynSent     = 0x01
  cast SynReceived = 0x02
  cast Established = 0x03
  cast Closing     = 0x04
  cast Closed      = 0x05
Cast Int StreamState where
  cast 0x00 = Initialised
  cast 0x01 = SynSent
  cast 0x02 = SynReceived
  cast 0x03 = Established
  cast 0x04 = Closing
  cast 0x05 = Closed
  cast _    = assert_unreachable

Show FrameType where
  show Data         = "Data"
  show WindowUpdate = "WindowUpdate"
  show Ping         = "Ping"
  show GoAway       = "GoAway"
Show FrameFlags where
  show Syn = "SYN"
  show Ack = "ACK"
  show Fin = "FIN"
  show Rst = "RST"
Show SessionType where
  show Client = "Client"
  show Server = "Server"
Show ProtoError where
  show Normal   = "Normal"
  show Protocol = "Protocol"
  show Internal = "Internal"
Show StreamState where
  show Initialised = "Initialised"
  show SynSent     = "SynSent"
  show SynReceived = "SynReceived"
  show Established = "Established"
  show Closing     = "Closing"
  show Closed      = "Closed"
Show ErrVal where
  show ev = show (returnVal ev) ++ ", errno=" ++ show (errnoVal ev)

