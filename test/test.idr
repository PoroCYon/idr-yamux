module Main

import Network.Socket
import Network.Yamux
import Network.Yamux.C
import System

type : SessionType
type = Client

initsock : SessionType -> Socket -> IO Int
initsock Client sock = do
  connect sock (IPv4Addr 164 132 96 43) 1337
initsock Server sock = do
  res <- bind sock (Just $ IPv4Addr 127 0 0 1) 1337
  if res == 0 then listen sock else pure res

main : IO ()
main = do
    r <- socket AF_INET Stream 6 -- tcp
    a r
  where
    b : Socket -> Int -> IO ()
    b sock 0 = do
      sess <- mkSession (MkConfig 0x80 0x1000) sock type
      r <- mkStream sess Nothing
      case r of
        Right str => do
          r' <- initStream str
          case r' of
            Nothing => do
              putStrLn $ show !(write str "hello from idris\n")
              putStrLn $ show !(read sess)
              usleep 1000000
              putStrLn $ show !(closeStream str)
              usleep 1000000
              putStrLn $ show !(goAway sess Normal)
            Just e => putStrLn $ "initStream failed: " ++ show e
        Left e => putStrLn $ "mkStream failed: " ++ show e
      usleep 1000000
      freeSession sess
    b _    e = putStrLn $ "init failed: " ++ show e
    a : Either Int Socket -> IO ()
    a (Right sock) = do
      initsock type sock >>= b sock
      r <- close sock
      pure ()
    a (Left  err ) = putStrLn $ "socket failed: " ++ show err

