# idr-yamux

An Idris port of Yamux. Currently, it doesn't start a new thread to check for incoming messages, nor does it automatically ping every so often to check for timeouts, etc.

This currently won't build with vanilla Idris, as it doesn't expose the file descriptors of a `Network.Socket.Socket`. If you want to try it, use [my fork](https://github.com/PoroCYon/Idris-dev) for now. A PR is sent (#3583).

## Usage

**NOTE**: the library isn't ready for use yet.

```idris
session <- mkSession config socket type
case !(mkStream session Nothing) of
    Right stream =>
        case !(initStream stream) of -- sends handshake
            Nothing => -- do stuff with stream...
            Just err => putStrLn $ show err
    Left err => putStrLn $ show err
```

## TODO

* Add event handling
* Add a receiving thread
* Implement timeout checking & pings
* Add LGPL file headers
